// Users

{
    "firstName": "Ponce",
    "lastName": "Jaen",
    "email": "nathanieljaen12@gmail.com",
    "password": "nathanieljaen12",
    "isAdmin": "false",
    "mobileNumber": "09664700055"
}


// Orders

{
    "userID": "1",
    "transactionDate": "03/27/2023",
    "status": "approved",
    "total": ""
}


// Products
{
    "name": "Mustang",
    "description": {
        "tank": "5.0L",
        "engine": "V8",
        "type": "AT Convertible"
    },
    "price": "3,818,00",
    "stocks": "2",
    "isActive": "true",
    "sku": "UGG-BB-PUR-07"
}

{
    "name": "Camaro",
    "description": {
        "tank": "6.2L",
        "engine": "V8",
        "type": "LT1 Convertible RWD"
    },
    "price": "2,382,538.88",
    "stocks": "1",
    "isActive": "true",
    "sku": "UGG-BB-PUR-08"
}



// Order Products
{
    "orderID": "1",
    "producID": "1",
    "quantity": "1",
    "price": "3,818,00",
    "subtotal": "3,818,00"
}
